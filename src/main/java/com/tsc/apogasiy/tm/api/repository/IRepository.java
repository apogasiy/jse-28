package com.tsc.apogasiy.tm.api.repository;

import com.tsc.apogasiy.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final @NotNull E entity);

    void addAll(final @NotNull List<E> entities);

    void remove(final @NotNull E entity);

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(@NotNull final Integer index);

    void clear();

    @Nullable
    E findById(@NotNull final String id);

    @Nullable
    E findByIndex(@NotNull final Integer index);

    @Nullable
    E removeById(@NotNull final String id);

    @Nullable
    E removeByIndex(@NotNull final Integer index);

    boolean isEmpty();

}
