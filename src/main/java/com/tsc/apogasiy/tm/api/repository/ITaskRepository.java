package com.tsc.apogasiy.tm.api.repository;

import com.tsc.apogasiy.tm.enumerated.Status;
import com.tsc.apogasiy.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @Nullable
    List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    Task findByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Task removeByName(@NotNull final String userId, @NotNull final String name);

    boolean existsById(@NotNull final String userId, @NotNull final String id);

    boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index);

    boolean existsByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Task startById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task startByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    Task startByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Task finishById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task finishByIndex(@NotNull final String userId, @NotNull final Integer index);

    @Nullable
    Task finishByName(@NotNull final String userId, @NotNull final String name);

    @Nullable
    Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status);

    @Nullable
    Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status);

    @Nullable
    Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status);

    @Nullable
    Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId);

    @Nullable
    Task unbindTaskById(@NotNull final String userId, @NotNull final String taskId);

    void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
