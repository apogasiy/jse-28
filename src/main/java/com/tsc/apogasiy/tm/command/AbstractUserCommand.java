package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.model.User;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    protected void showUser(@NotNull final User user) {
        System.out.println("Id: " + user.getId());
        System.out.println("Login: " + user.getLogin());
        System.out.println("Full name: " + getFullName(user));
        System.out.println("Email: " + user.getEmail());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @NotNull
    private String getFullName(@NotNull final User user) {
        @NotNull String fullName = "";
        if (!user.getLastName().isEmpty())
            fullName = user.getLastName();
        if (!user.getFirstName().isEmpty()) {
            if (fullName.isEmpty())
                fullName = user.getFirstName();
            else
                fullName = fullName + " " + user.getFirstName();
        }
        if (!user.getMiddleName().isEmpty()) {
            if (fullName.isEmpty())
                fullName = user.getMiddleName();
            else
                fullName = fullName + " " + user.getMiddleName();
        }
        return fullName;
    }

}
