package com.tsc.apogasiy.tm.command;

import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import com.tsc.apogasiy.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    @NotNull
    protected Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty())
            throw new EmptyNameException();
        return new Project(userId, name, description);
    }

}
