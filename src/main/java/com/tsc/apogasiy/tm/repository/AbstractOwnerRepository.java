package com.tsc.apogasiy.tm.repository;

import com.tsc.apogasiy.tm.api.repository.IOwnerRepository;
import com.tsc.apogasiy.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    @NotNull
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return findAll(userId).stream()
                .filter(entity -> entity.getId().equals(id))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull List<E> userList = findAll(userId);
        if (index < 0 || index >= userList.size())
            return null;
        return userList.get(index);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull List<String> entityUserId = list.stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        entityUserId.forEach(list::remove);
    }

    @Override
    @Nullable
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final E entity = findById(userId, id);
        if (!Optional.ofNullable(entity).isPresent())
            return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    @Nullable
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final E entity = findByIndex(userId, index);
        if (!Optional.ofNullable(entity).isPresent())
            return null;
        remove(userId, entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final List<E> list = findAll(userId);
        list.remove(entity);
    }

    @NotNull
    public Integer getSize(@NotNull final String userId) {
        @NotNull final List<E> list = findAll(userId);
        return list.size();
    }

}
