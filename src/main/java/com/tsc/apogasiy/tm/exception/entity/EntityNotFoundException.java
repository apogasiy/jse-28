package com.tsc.apogasiy.tm.exception.entity;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
