package com.tsc.apogasiy.tm.exception.empty;

import com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty!");
    }

}
