package com.tsc.apogasiy.tm.component;

import com.tsc.apogasiy.tm.api.repository.*;
import com.tsc.apogasiy.tm.api.service.*;
import com.tsc.apogasiy.tm.command.AbstractCommand;
import com.tsc.apogasiy.tm.enumerated.Role;
import com.tsc.apogasiy.tm.exception.system.UnknownCommandException;
import com.tsc.apogasiy.tm.model.Project;
import com.tsc.apogasiy.tm.model.Task;
import com.tsc.apogasiy.tm.model.User;
import com.tsc.apogasiy.tm.repository.*;
import com.tsc.apogasiy.tm.service.*;
import com.tsc.apogasiy.tm.util.HashUtil;
import com.tsc.apogasiy.tm.util.SystemUtil;
import com.tsc.apogasiy.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService);

    public void start(final String... args) {
        initPID();
        displayWelcome();
        runArgs(args);
        initCommands();
        initTestData();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.tsc.apogasiy.tm.command");
        for (@NotNull final Class<? extends AbstractCommand> clazz : reflections.getSubTypesOf(AbstractCommand.class)) {
            if (!Modifier.isAbstract(clazz.getModifiers())) {
                try {
                    registry(clazz.newInstance());
                } catch (InstantiationException | IllegalAccessException e) {
                    logService.error(e);
                }
            }
        }
    }

    private void initTestData() {
        createDefaultUser();
        createDefaultData();
    }

    private void createDefaultData() {
        @NotNull final String defaultUserId = userService.findByLogin("user").getId();
        @NotNull final String defaultAdminId = userService.findByLogin("admin").getId();
        projectService.add(new Project(defaultAdminId, "Project 1", "-"));
        projectService.add(new Project(defaultAdminId, "Project 2", "-"));
        projectService.add(new Project(defaultUserId, "Project 3", "-"));
        projectService.add(new Project(defaultUserId, "Project 4", "-"));
        taskService.add(new Task(defaultAdminId, "Task 1", "Default Task"));
        taskService.add(new Task(defaultAdminId, "Task 2", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 3", "Default Task"));
        taskService.add(new Task(defaultUserId, "Task 4", "Default Task"));
        taskService.findByName(defaultAdminId, "Task 1").setProjectId(projectService.findByName(defaultAdminId, "Project 1").getId());
        taskService.findByName(defaultAdminId, "Task 2").setProjectId(projectService.findByName(defaultAdminId, "Project 2").getId());
        taskService.findByName(defaultUserId, "Task 3").setProjectId(projectService.findByName(defaultUserId, "Project 3").getId());
        taskService.findByName(defaultUserId, "Task 4").setProjectId(projectService.findByName(defaultUserId, "Project 4").getId());
    }

    private void createDefaultUser() {
        @NotNull final User user = new User("user", HashUtil.encrypt(getPropertyService(), "user"));
        user.setRole(Role.USER);
        user.setEmail("user@folder.com");
        userService.add(user);
        @NotNull final User admin = new User("admin", HashUtil.encrypt(getPropertyService(), "admin"));
        admin.setRole(Role.ADMIN);
        user.setEmail("admin@folder.com");
        userService.add(admin);
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private boolean runArgs(final String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0)
            return false;
        @Nullable AbstractCommand command = commandService.getCommandByName(args[0]);
        if (!Optional.ofNullable(command).isPresent())
            throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty())
            return;
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent())
            throw new UnknownCommandException(command);
        @Nullable final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
